import 'package:flutter/material.dart';

class ContadorPage extends StatefulWidget {
  createState() => _ContadorPageState();
}

class _ContadorPageState extends State<ContadorPage> {
  final estiloTexto = new TextStyle(fontSize: 25, fontWeight: FontWeight.bold);
  final estiloTextoTitulo = new TextStyle(fontSize: 28);
  int _conteo = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Contador Basico'),
          centerTitle: true,
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text('Una app para entender Flutter',
                      style: estiloTextoTitulo),
                ],
              ),
              SizedBox(
                height: 30,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text('Numero de clicks:', style: estiloTexto),
                ],
              ),
              SizedBox(height: 5),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text('$_conteo', style: estiloTexto),
                ],
              ),
            ],
          ),
        ),
        floatingActionButton: _crearBotones());
  }

  Widget _crearBotones() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        SizedBox(
          width: 30.0,
        ),
        FloatingActionButton(
          child: Icon(Icons.exposure_zero),
          onPressed: _reset,
        ),
        Expanded(
            child: SizedBox(
          width: 5.0,
        )),
        FloatingActionButton(
          child: Icon(Icons.remove),
          onPressed: _sustrar,
        ),
        SizedBox(
          width: 5.0,
        ),
        FloatingActionButton(
          child: Icon(Icons.add),
          onPressed: _agregar,
        ),
      ],
    );
  }

  void _agregar() => setState(() => _conteo++);

  void _reset() => setState(() => _conteo = 0);

  void _sustrar() => setState(() {
        if (_conteo < 1) {
          _conteo = 0;
        } else {
          _conteo--;
        }
      });
}
